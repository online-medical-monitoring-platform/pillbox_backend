package ro.tuc.ds2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.validation.annotation.Validated;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.services.PillBoxInterface;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;

import java.util.List;
import java.util.TimeZone;

@SpringBootApplication
@Validated
public class Ds2020Application extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Ds2020Application.class);
    }


    @Bean
    public HessianProxyFactoryBean hessianInvoker() {
        HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
        //invoker.setServiceUrl("http://localhost:8080/hellohessian");
        invoker.setServiceUrl("https://serban-codruta-tema3.herokuapp.com/hellohessian");
        invoker.setServiceInterface(PillBoxInterface.class);
        return invoker;
    }

    public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        ConfigurableApplicationContext context =  SpringApplication.run(Ds2020Application.class, args);
        PillBoxInterface service =  context.getBean(PillBoxInterface.class);
       /* System.out.println(service.sayHelloWithHessian("Sajal"));
        PatientDTO patient=service.findPatientById((long) 376);
        System.out.println(patient.getId());*/


    }
}
