package ro.tuc.ds2020.controllers;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPrescriptionDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.UserAppDTO;
import ro.tuc.ds2020.services.PillBoxInterface;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = "/pillbox")
public class PillBoxController {

    PillBoxInterface service;
    public PillBoxController(PillBoxInterface service){
        this.service=service;
    }

    //ia din medication plan
    @GetMapping(value = "/medicationPlan/{id}/medicationPrescription")
    public ResponseEntity<List<MedicationPrescriptionDTO>> getMedicationPrescriptions(@PathVariable("id") Long medicationPlanId) {
        List<MedicationPrescriptionDTO> plans= service.getMedicationPrescriptions(medicationPlanId);
        if(plans==null || plans.size() ==0)
            plans= new ArrayList<MedicationPrescriptionDTO>();

        return new ResponseEntity<>(plans, HttpStatus.OK);
    }

    //cauta in userapp e pt login
    @GetMapping(value = "/user/{username}/{password}")
    public ResponseEntity<UserAppDTO> getPatientByUser(@PathVariable("username") String username, @PathVariable("password") String password) {
        UserAppDTO user = service.findUserByUsernameAndPassword(username, password);
        PatientDTO patient=service.findPatientById(user.getId());
        System.out.println("Username si parola :"+username+ "  "+ password);
        System.out.println("Pacient gasit"+ user);
        if(!user.getRole().equalsIgnoreCase("patient"))
            user.setRole("notFound");
        return new ResponseEntity<>(user, HttpStatus.OK);

    }

    //update pt medicationPrescription daca o luat pastila sau nu
    @PutMapping(value = "/medicationPrescription/{id}")
    public ResponseEntity<Long> update(@PathVariable("id") Long id, @Valid @RequestBody MedicationPrescriptionDTO medicationPrescriptionUpdateDTO) {
        Long medicationPrescriptionID = service.update(id, medicationPrescriptionUpdateDTO);
        return new ResponseEntity<>(medicationPrescriptionID, HttpStatus.CREATED);
    }


    //dupa logare ii afisez datele pacientului pe pagina
    @GetMapping(value = "/patient/{id}")
    public ResponseEntity<PatientDTO> getPatient(@PathVariable("id") Long patientId) {
        PatientDTO dto = service.findPatientById(patientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    //extrag medication plans pt un anumit patient
    @GetMapping(value = "/patient/{id}/medicationPlan")
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlan(@PathVariable("id") Long patientId) {
        List<MedicationPlanDTO> plans= service.getMedicationPlan(patientId);
        if(plans==null || plans.size() ==0)
            plans= new ArrayList<MedicationPlanDTO>();

        return new ResponseEntity<>(plans, HttpStatus.OK);
    }



}
