package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.Objects;

public class MedicationPlanDTO extends RepresentationModel<MedicationPlanDTO> implements Serializable {
    private Long id;

    private String name;
    private String periodStart;
    private String periodStop;

    private PatientDTO patient;


    public MedicationPlanDTO() {
    }

    public MedicationPlanDTO(Long id, String name,String start,String stop,PatientDTO patient) {
        this.id=id;
        this.name=name;
        this.periodStart=start;
        this.periodStop=stop;
        this.patient=patient;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PatientDTO getPatient() {
        return patient;
    }

    public void setPatient(PatientDTO person) {
        this.patient = person;
    }

    public String getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(String periodStart) {
        this.periodStart = periodStart;
    }

    public String getPeriodStop() {
        return periodStop;
    }

    public void setPeriodStop(String periodStop) { this.periodStop = periodStop; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlanDTO medicationPlanDTO = (MedicationPlanDTO) o;
        return Objects.equals(name, medicationPlanDTO.name) &&
                Objects.equals(periodStart, medicationPlanDTO.periodStart) &&
                Objects.equals(periodStop, medicationPlanDTO.periodStop) &&
                Objects.equals(patient, medicationPlanDTO.patient);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name,periodStart,periodStop,patient);
    }

}
