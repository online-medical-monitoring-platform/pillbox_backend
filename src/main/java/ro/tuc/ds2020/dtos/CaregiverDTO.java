package ro.tuc.ds2020.dtos;

        import org.springframework.hateoas.RepresentationModel;


        import java.io.Serializable;
        import java.util.Objects;
        import java.util.stream.Collectors;

public class CaregiverDTO extends RepresentationModel<CaregiverDTO> implements Serializable {
    private Long id;
    private String username;
    private String password;
    private String name;
    private String birthdate;
    private String gender;
    private String address;
    private String role;

    public CaregiverDTO() {
    }

    public CaregiverDTO(Long id,String username,String password,  String name, String birthdate, String gender, String address ) {
        this.id = id;
        this.username=username;
        this.password=password;
        this.name = name;
        this.birthdate = birthdate;
        this.gender= gender;
        this.address= address;
        this.role="caregiver";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO caregiverDTO = (CaregiverDTO) o;
        return  Objects.equals(username, caregiverDTO.getUsername()) &&
                Objects.equals(password, caregiverDTO.getPassword()) &&
                Objects.equals(name, caregiverDTO.name) &&
                Objects.equals(birthdate, caregiverDTO.birthdate) &&
                Objects.equals(gender, caregiverDTO.gender) &&
                Objects.equals(address, caregiverDTO.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birthdate, gender,address);
    }


    public String toStringCaregiverDTO(){
        return "Caregiver id:"+ id+" and name "+name;
    }
}
